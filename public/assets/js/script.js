//conditional statements are statements that help our program decide what to do
/*
There are three main types of conditional statements
1. if-else statements
2. switch statements
3. try-catch-finally
*/

let age =10;
if (age >= 18) {
	console.log("You may buy wine");
}	else {
	console.log("You can not buy wine");
}

//multiple conditions

let age2 = 19;
let isFullTime = false;

if (age2 >= 18 && !isFullTime) {
	console.log("can enroll to bootcamp day class");
} else {
	console.log("can enroll to bootcamp night class");
}

// if else chaining - this is done to combine several possible outcomes
/*
if money is greater than 500 - take a grab
if money is between 100 to 500 - take a taxi
if money is between 50 to 100 - take a jeep
if money is between 1 to 50 - take a walk
else - stay at home
*/

let paoMoney = 100;

if (paoMoney > 500) {
	console.log("Will ride a grab");
} else if (paoMoney > 100 && paoMoney <= 500) {
	console.log("Will ride a taxi");
} else if (paoMoney > 50 && paoMoney <= 100) {
	console.log("Will ride a jeep");
} else if (paoMoney > 1 && paoMoney <= 50) {
	console.log("Will ride a jeep");
} else {
	console.log("Stay home");
}

//create a function that would test if a number is odd or even
function testOddEven(num){
	if (num % 2 == 0) {
		return "even";
	} else {
		return "odd";
	}
}

console.log(testOddEven(78));
console.log(testOddEven(99523));
console.log(testOddEven(955));

//mini exercise

function diffTester(num1, num2){
	if (num1 - num2 > 0) {
		return "positive"; 
	} else if (num1 - num2 <0){
		return "negative";
	} else {
		return "zero";
	}
}

//sirT solution
/*
function diffTester(num1, num2){
	let difference = num1 - num2;
	if (difference > 0){
		return "positive";
	} else if (difference < 0) {
		return "negative";
	} else {
		return "zero";
	}
}
*/

console.log(diffTester(10, 8));
console.log(diffTester(87, 96));
console.log(diffTester(8, 8));

//create function that would check if the password has 8 characters
function passwordLengthCheck(password){
	if (password.length >= 8){ // .length is a propert of strings that count how many chars are there in a string
		return "Password length is ok";
	} else {
		return "Password too short";
	}
}

console.log(passwordLengthCheck("test1234"));
console.log(passwordLengthCheck("1234"));


//switch statements are used for deciding from discrete valuess (the values to choose from are distinct)
let directionNumber = 1;

switch(directionNumber) {
	case 1:
		console.log("north");
		break;
	case 2:
		console.log("east");
		break;
	case 3:
		console.log("west");
		break;
	case 4:
		console.log("south");
		break;
	default:
	console.log("no matching direction");
}

/*
syntax:

switch(variable to check) {
	case possibleValue1:
		//do something here..
		break;
	case possibleValue2:
		//do something here..
		break;
	...
	default:
		//do something here..
}

each case corresponds to a possible value
the break statement makes sure that the case is finished
the default statement is the fallback if there are no cases that satisfy
*/

//create a switch statement that takes a letter (A, B, C) and determines what meal to eat

let choice = "a";

switch (choice.toUpperCase()) {
	case "A":
//	case "a": // you can have more cases as needed and have the same output
	console.log("You have chosen breakfast");
		break;
	case "B":
//	case "b":
	console.log("You have chosen lunch");
		break;
	case "C":
	console.log("You have chosen dinner");
		break;
	default:
	console.log("No more than 3 meals a day");
}

/*
we can solve it two ways
1. add another case for lower case letters
2.  manipulate teh variable to be upper case at all times
*/

//Mini exercise: create a switch statement that asks for a day (1 to 12) then outputs the corresponding gift
//ex.

let day = 4;
switch (day) {
	case 1:
		console.log("A partridge in a pear tree,");
		break;
	case 2:
		console.log("Two turtle doves,");
		brak;
	case 3:
		console.log("Three french hens,");
		break;
	case 4:
		console.log("Four calling birds,");
		break;
	case 5:
		console.log("Five gold rings,");
		break;
	case 6:
		console.log("Six geese a-laying");
		break;
	case 7:
		console.log("Seven swans a-swimming,");
		break;
	case 8:
		console.log("Eight maids a-milking,");
		break;
	case 9:
		console.log("Nine ladies dancing,");
		break;	
	case 10:
		console.log("Ten lords a-leaping,");
		break;	
	case 11:
		console.log("Eleven pipers piping,");
		break;
	case 12:
		console.log("Twelve drummers drumming.");
		break;
	default:
		console.log("no gift");
}
//uncaught syntax error: missing chatch or finally after try
//try-catch-finally statement is used to run codes and catch error messages
try {
	//code to try executing, this part would be the code that has a chance of failing
	//examples would be alerts, computations, opening files
	alerat("Hello World");
} catch (error) { //err is a variable that catches the error, it is user defined and can have whatever name yu want
	console.log(error.message); //.message is a property that shows what error message was encountered
}	finally {
	alert ("This runs regardless if the try block succeeds or fails.");
	//any code put in the finally block will run regardless if the try succeeds or fails
}


//ternary operator -> a shorthand of our if-else statement
/*
Syntax:
if else statement
if (condition/s) {
	do this if the conditions are true	
} else {
	do this if the conditions are false
}

ternary statement
(condition) ? value if true : value if false
Note however that it only outputs values
*/

//Recall code for odd even test?
let numToTest = 67;
if (numToTest % 2 == 0) {
	console.log ("even");
} else {
	console.log ("odd");
}

function oddEvenTest2(number){
	return (number % 2 == 0) ? "even" : "odd";
}

console.log(oddEvenTest2(numToTest));

//mini exercise:
//create a function that gets a number and doubles it if it is odd and halves if it is even.

/*
test cases:
15->30
656 -> 328
99 -> 198
80 -> 40
*/


function doubleHalf(num){
	return (num % 2 == 0) ? num / 2 : num * 2;
}

console.log(doubleHalf(15));
console.log(doubleHalf(656));
console.log(doubleHalf(99));
console.log(doubleHalf(80));


//create a function that gets two numbers and a letter (a and b), if "a" is chosen, add the two numbers,
// if "b" is chosen, subtract the two numbers

/*
(1, 1, "a") -> 2
(7, 3, "b") -> 4
(7, 8, "c") -> "Invalid letter"
*/

// function (num1, num2,){
// 	if ("a") {
// 		return num1 + num2;
// 	} else if ("b") {
// 		return num1 - num2;
// 	} else {
// 		return "invalid letter";
// 	}
// }

// console.log(b(1, 1, "a"));
// console.log(b(7, 3, "b"));
// console.log(b(7, 8, "c"));


function addSub(num1, num2, letter){
	let letterCleaned = letter.toLowerCase();
	if(letterCleaned == "a"){
		return num1 + num2;
	} else if (letterCleaned == "b"){
		return num1 - num2;
	} else {
		return "Invalid letter";
	}
}

console.log(addSub(1, 1, "a"));
console.log(addSub(7, 3, "b"));
console.log(addSub(7, 8, "c"));
